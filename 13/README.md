``` bash
# API
cd Api

# Install all dependency
npm install

# Run the Client
node server.js

# BASH
cd ../Bash

# Run bash file
./bash.sh
```

## Contributors
- Luc Nabhan
- Geoffrey Dalfin