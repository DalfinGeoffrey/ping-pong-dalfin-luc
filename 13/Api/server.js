var http = require('http');
var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var cpt = 0;
var sentryid;
var Raven = require('raven');
var readline = require('readline');
// var rl = readline.createInterface({
//     input: process.stdin,
//     output: process.stdout
//   });
//   rl.question("Entrer votre dsn sentry? ", function(answer) {
//     console.log(answer);
//     sentryid = answer;
//     rl.close();
//   });
  //https://e5916e6e3a7b408a93861b14f1e4e5f2:5c10cd7d0eb4430283bc4683f26a3dae@sentry.io/258227
// Raven.config(sentryid).install();
// app.use(Raven.requestHandler());
// app.get('/', function mainHandler(req, res) {
//     throw new Error('erreur');
// });

app.get('/ping', function mainHandler(req, res) {
    res.contentType('application/json');
    res.json({
        message: "pong"
    })
    cpt++;
    res.end();
});

app.get('/count', function mainHandler(req, res) { 
    res.setHeader('Content-Type', 'text/json');
    res.json({
        pingCount: cpt
    })
    console.log(res)
    res.end();
});

// app.use(function onError(err, req, res, next) {
//     res.statusCode = 500;
//     res.end(res.sentry + '\n');
// });

// app.use(Raven.errorHandler());

app.listen(port);